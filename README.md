# Automate Job Applications
A small script to automate some of the boilerplate process of applying for jobs.
Intention is to make it easier to focus on fine tuning applications.
(User input is made possible via [Zenity](https://help.gnome.org/users/zenity/stable/index.html.en))

There are several options for a CV:
+ Double paged based on [modernCV](https://www.ctan.org/pkg/moderncv) (Old)
+ Single page made with [Tikz](https://tikz.dev/) based on a [blog post by Olivier Pieters](https://olivierpieters.be/blog/2017/09/12/designing-a-cv-in-latex-part-1).
+ Two page CV: First as previous and second page including Academic activities.

Next to the format of the CV it also provides the option to select different `About Me` sections and different boilerplate sentences for the Cover-letter.

