#!/bin/bash

# set the separator
IFS=";"

#======================user input=====================================
# get some basic info
project_name=$(zenity --entry --text="Project Name" --entry-text="")
# check if project already exists
# create folder structure
if [ -d "$project_name" ]; then
    zenity --error --text="Project already exists"
    exit 1
else
    mkdir $project_name
    mkdir $project_name/CV
    mkdir $project_name/Coverletter
fi

# continue with input
position=$(zenity --entry --text="Position" --entry-text="")
cooperation=$(zenity --entry --text="Name of the Cooperation" --entry-text="")
link=$(zenity --entry --text="Link to the Position" --entry-text="")

single_double=$(zenity --list --radiolist  --column="" --column="Single or Double" True  single False double False singlePlus)
echo $single_double
# get a list of skills
if [[ $single_double == "double" ]];
then
    # grab a skill selection
    selection=$(zenity --list --checklist --separator=";" --column="" --column="skill" True python True matlab True bash False rust False "Cookie-Fairy")
else
    # create a selection of possible about me sections
    mes=()
    for me in template/taglines/*;
    do
	if [[ $me == *.tex ]];
	then
	    temp="${me%.tex}"  # remove suffix
	    mes+=("${temp##*/}")
	fi
    done
    me_select=()
    for me in ${mes[@]};
    do
	me_select+=("False")
	me_select+=("$me")
    done
    me_select[0]="True"
    me_selection=$(zenity --list --radiolist --column="" --column="Tagline Selection" "${me_select[@]}")
fi

# get the list of text snippets
snips=()
for snip in template/snippets/*;
do
    if [[ $snip == *.tex ]];
    then
	temp="${snip%.tex}"  # remove file extension suffix
	snips+=("${temp##*/}") # remove path prefix
    fi
done
# create snippet selection array for zenity
snip_select=()
for snip in ${snips[@]};
do
    snip_select+=("False")
    snip_select+=("$snip")
done
snippet_selection=$(zenity --list --checklist --separator=";" --column="" --column="Text Snippets" "${snip_select[@]}")

#===================user input ends====================================


# create the README file
echo "# Application: ${position} at ${cooperation}" >> $project_name/README.MD
echo "Applying as a [${position}](${link})" >> $project_name/README.MD

if [[ $single_double == "double" ]];
then
    # copy template files
    cp -r template/CV/*  $project_name/CV
    cp -r template/Coverletter/* $project_name/Coverletter
    # create the cs_skills.tex file !! only possible for the double sided thing
    for skill in $selection
    do
	cat template/skills/${skill}.tex >> $project_name/CV/cs_skills.tex
    done
else
    if [[ $single_double == "singlePlus" ]];
    then
	cp -r template/CV_single_plus/*  $project_name/CV
	cp -r template/Coverletter_single/* $project_name/Coverletter
	cp template/taglines/${me_selection}.tex $project_name/CV/tagline.tex
    else
	cp -r template/CV_single/*  $project_name/CV
	cp -r template/Coverletter_single/* $project_name/Coverletter
	cp template/aboutMes/${me_selection}.tex $project_name/CV/about_me.tex
    fi
fi

# create the main_text.tex file
for snip in $snippet_selection
do
    cat template/snippets/${snip}.tex >> $project_name/Coverletter/main_text.tex
done

# replace the position/cooperation place holder in the coverletter
sed -i "s/&Position&/${position}/g" $project_name/Coverletter/Coverletter.tex
sed -i "s/&cooperation&/${cooperation}/g" $project_name/Coverletter/Coverletter.tex


# compile pdf files
cd $project_name/CV/

pdflatex CV.tex
if [[ $single_double == "singlePlus" ]];
then
    bibtex CV
fi
sleep 1 # I have no clue why but I need to wait a second before recompiling it.
pdflatex CV.tex

cd ../Coverletter/
    
pdflatex Coverletter.tex
sleep 1
pdflatex Coverletter.tex

# provide me with a link formated for org files.
zenity --info  --text="[[${link}][${position} at ${cooperation}]] ([[./automate_applications/${project_name}/CV/CV.tex][CV]], [[./automate_applications/${project_name}/Coverletter/Coverletter.tex][Coverletter]])"
